import Vue from 'vue'
import Vuex from 'vuex'
import { firestorePlugin } from 'vuefire'
import firebase from 'firebase/app'
import 'firebase/firestore'
import { shuffle } from '@/utils'

// connect to firebase bd
firebase.initializeApp({
  projectId: 'catsmash-13641',
  databaseURL: 'https://catsmash-13641.firebaseio.com'
})
const db = firebase.firestore()

Vue.use(firestorePlugin)
Vue.use(Vuex)
const store = new Vuex.Store({
  strict: false,
  state: {
    listCats: []
  },
  mutations: {
    updateList (state, listCats) {
      state.listCats = shuffle(listCats)
    },
    addVote (state, id) {
      state.listCats.map((item) => {
        if (item.id === id) {
          if (item.nbVote) {
            item.nbVote = parseInt(item.nbVote) + 1
          } else {
            item['nbVote'] = 1
          }
          db.collection('cats')
            .doc(id)
            .update(item)
            .then((err) => {
              console.log('user updated!', err)
            })
        }
      })
    }
  },
  actions: {
    updateList ({ commit }, listCats) {
      if (listCats) {
        commit('updateList', listCats)
      }
    },
    addVote ({ commit, state }, id) {
      if (id) {
        commit('addVote', id)
      }
    }
  }
})

db.collection('cats')
  .get()
  .then((querySnapshot) => {
    const list = []
    querySnapshot.forEach(function(doc) {
      list.push(doc.data())
    })
    store.dispatch('updateList', list)
  })

export default store
